def split_image(img,split_parts):
    w,h = img.size
    pw = w/int(split_parts)
    ph = h/int(split_parts)
    return_list = []
    for idx in xrange (0, w, pw):
        for jdx in xrange(0, h, ph):
            return_list.append(img.crop((idx, jdx, idx+pw, jdx+ph)))
    return return_list

def hash_similar(new_img):
    global old_img_key
    new_img_gray = new_img.convert('L').resize((9,8))
    new_img_key = []
    for idx in xrange(new_img_gray.size[0]):
        for jdx in xrange(new_img_gray.size[1]-1):
            if new_img_gray.getpixel((idx,jdx)) > new_img_gray.getpixel((idx,jdx+1)):
                new_img_key.append(1)
            else:
                new_img_key.append(0)
    cache_similarity = 0.0
    for idx in xrange(len(new_img_key)):
        if old_img_key[idx] == new_img_key[idx]:
            cache_similarity += 1.0
    return cache_similarity/64.0

def pix_similar(new_img,original_img):
    new_img_RGB = new_img.resize((int(new_img.size[0]/3),int(new_img.size[1]/2))).convert('RGB')
    old_img_RGB = original_img.resize((int(new_img.size[0]/3),int(new_img.size[1]/2))).convert('RGB')
    similarity_sum = sum(1.0-(abs(new_color - old_color)/255.0) for x in xrange(new_img_RGB.size[0]) for y in xrange(new_img_RGB.size[1])\
     for old_color,new_color in zip(old_img_RGB.getpixel((x,y)),new_img_RGB.getpixel((x,y))))
    return similarity_sum/(new_img_RGB.size[0]*new_img_RGB.size[1]*3.0)

def compare(shell,original_img):
    shell.is_mutate()
    if shell.is_changed():
        shell.set_similarity(100.0*pix_similar(shell.get_img(),original_img))
        shell.compared()
    return shell
